package ru.korzhavin.filestorageapi.controller;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.korzhavin.filestorageapi.model.FileInfo;
import ru.korzhavin.filestorageapi.service.FileService;

import static ru.korzhavin.filestorageapi.controller.FileController.BASE_URL;

@RestController
@RequestMapping(BASE_URL)
public class FileController {

    public static final String BASE_URL = "/file";
    public static final String UPLOAD_URL = "/upload";
    public static final String DELETE_URL = "/delete";
    public static final String GET_URL = "/get";

    @Autowired
    private FileService fileService;

    @PutMapping(UPLOAD_URL)
    public ResponseEntity<FileInfo> upload(@RequestBody @NonNull MultipartFile file) {
        return ResponseEntity.ok(fileService.upload(file));
    }

    @DeleteMapping(DELETE_URL)
    public ResponseEntity<String> delete(@RequestParam(value = "name") @NonNull String systemName) {
        fileService.delete(systemName);
        return ResponseEntity.ok("Success");
    }

    @GetMapping(GET_URL)
    public ResponseEntity<InputStreamResource> get(@RequestParam(value = "name") @NonNull String systemName) {
        return fileService.getFile(systemName);
    }

}
