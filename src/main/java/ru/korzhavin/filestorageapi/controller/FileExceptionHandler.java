package ru.korzhavin.filestorageapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.korzhavin.filestorageapi.exceptions.FileException;

@RestControllerAdvice
public class FileExceptionHandler {

    @ExceptionHandler(FileException.class)
    public ResponseEntity<String> handle(FileException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

}
