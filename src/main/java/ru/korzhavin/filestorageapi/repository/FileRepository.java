package ru.korzhavin.filestorageapi.repository;

import ru.korzhavin.filestorageapi.model.FileInfo;

public interface FileRepository {

    void save(FileInfo fileInfo);
    FileInfo get(String systemName);
    FileInfo delete(String systemName);
}
