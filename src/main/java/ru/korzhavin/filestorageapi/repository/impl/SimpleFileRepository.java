package ru.korzhavin.filestorageapi.repository.impl;

import org.springframework.stereotype.Service;
import ru.korzhavin.filestorageapi.model.FileInfo;
import ru.korzhavin.filestorageapi.repository.FileRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SimpleFileRepository implements FileRepository {

    private static Map<String, FileInfo> storage = new ConcurrentHashMap<>(1_000_000);

    @Override
    public void save(FileInfo fileInfo) {
        storage.put(fileInfo.getSystemName(), fileInfo);
    }

    @Override
    public FileInfo get(String systemName) {
        return storage.get(systemName);
    }

    @Override
    public FileInfo delete(String systemName) {
        return storage.remove(systemName);
    }
}
