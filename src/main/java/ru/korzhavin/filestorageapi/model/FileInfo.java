package ru.korzhavin.filestorageapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;

import java.nio.file.Path;

@Getter
public class FileInfo {

    private String systemName;

    @JsonIgnore
    private String originName;

    private Path path;

    @JsonIgnore
    private String contentType;

    public FileInfo(@NonNull String systemName, @NonNull String originName, @NonNull Path path, @NonNull String contentType) {
        this.systemName = systemName;
        this.originName = originName;
        this.path = path;
        this.contentType = contentType;
    }
}
