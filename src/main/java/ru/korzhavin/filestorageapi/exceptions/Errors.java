package ru.korzhavin.filestorageapi.exceptions;

public enum Errors {
    FILE_NOT_FOUND("File not found"),
    FILE_NOT_SAVE("File not saved");

    private final String text;

    Errors(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
