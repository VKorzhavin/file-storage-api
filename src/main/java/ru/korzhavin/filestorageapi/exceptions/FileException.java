package ru.korzhavin.filestorageapi.exceptions;

public class FileException extends RuntimeException {

    public FileException(String message) {
        super(message);
    }

    public FileException(Errors error, Throwable cause) {
        super(error.getText(), cause);
    }

    public FileException(Throwable cause) {
        super(cause);
    }
}
