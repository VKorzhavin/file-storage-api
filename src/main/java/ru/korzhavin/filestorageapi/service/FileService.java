package ru.korzhavin.filestorageapi.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import ru.korzhavin.filestorageapi.model.FileInfo;

public interface FileService {

    FileInfo upload(MultipartFile file);
    void delete(String systemName);
    ResponseEntity<InputStreamResource> getFile(String systemName);

}
