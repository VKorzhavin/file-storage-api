package ru.korzhavin.filestorageapi.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.korzhavin.filestorageapi.exceptions.Errors;
import ru.korzhavin.filestorageapi.exceptions.FileException;
import ru.korzhavin.filestorageapi.model.FileInfo;
import ru.korzhavin.filestorageapi.repository.FileRepository;
import ru.korzhavin.filestorageapi.service.FileService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Service
@PropertySource("classpath:application.properties")
public class SimpleFileService implements FileService {

    @Autowired
    private FileRepository fileRepository;

    @Value("${directory.base}")
    private String BASE_DIRECTORY;

    @Value("${directory.limit}")
    private int FILE_LIMIT;

    private AtomicLong counter = new AtomicLong(0);
    private String subDirectory = String.valueOf(System.currentTimeMillis());

    @Override
    public FileInfo upload(MultipartFile file) {
        log.debug(String.format("Start save file %s ", file.getOriginalFilename()));
        long temp = counter.getAndIncrement();
        String systemName = generateSystemName(temp);
        FileInfo fileInfo;
        try {
            fileInfo = new FileInfo(
                systemName,
                file.getOriginalFilename(),
                Paths.get(createAndGetDirectory(temp), systemName),
                file.getContentType()
            );
            Files.copy(file.getInputStream(), fileInfo.getPath());
            fileRepository.save(fileInfo);
            log.debug(String.format("File %s saved to %s", file.getOriginalFilename(), fileInfo.getPath()));
        } catch (Exception ex) {
            log.error(String.format("File %s not saved", file.getOriginalFilename()), ex);
            throw new FileException(Errors.FILE_NOT_SAVE, ex.getCause());
        }
        return fileInfo;
    }

    @Override
    public void delete(String systemName) {
        FileInfo fileInfo = fileRepository.delete(systemName);
        try {
            Files.delete(fileInfo.getPath());
        } catch (Exception ex) {
            log.error(String.format("Error on delete file systemName %s ", systemName), ex);
            throw new FileException(Errors.FILE_NOT_FOUND, ex);
        }
    }

    public ResponseEntity<InputStreamResource> getFile(String systemName) {
        FileInfo fileInfo;
        ResponseEntity<InputStreamResource> responseEntity;
        try {
            fileInfo = fileRepository.get(systemName);
            File file = new File(fileInfo.getPath().toString());
            responseEntity = ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + URLEncoder.encode(fileInfo.getOriginName(), "UTF-8"))
                    .contentType(MediaType.parseMediaType(fileInfo.getContentType()))
                    .contentLength(file.length())
                    .body(new InputStreamResource(new FileInputStream(file)));
        } catch (Exception ex) {
            log.error(String.format("Error on get file systemName %s ", systemName), ex);
            throw new FileException(Errors.FILE_NOT_FOUND, ex);
        }
        return responseEntity;
    }

    private String createAndGetDirectory(long directoryNumber) throws IOException {
        Path path = Paths.get(
                BASE_DIRECTORY, subDirectory, String.valueOf(Math.round(directoryNumber / FILE_LIMIT))
        );
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        return path.toString();
    }

    private String generateSystemName(long counter) {
        return System.currentTimeMillis() + "-" + counter;
    }
}
